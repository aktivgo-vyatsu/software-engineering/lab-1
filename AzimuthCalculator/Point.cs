namespace AzimuthCalculator;

public class Point
{
    public double Latitude { get; }
    public double Longitude { get; }
    
    public Point(double latitude, double longitude)
    {
        Validate(latitude, longitude);
        Latitude = latitude;
        Longitude = longitude;
    }

    private static void Validate(double latitude, double longitude)
    {
        if (longitude < -180 || longitude > 180)
        {
            throw new ArgumentOutOfRangeException("Значение широты должно быть от -180 до 180");
        }
            
        if (latitude < -90 || latitude > 90)
        {
            throw new ArgumentOutOfRangeException("Значение долготы должно быть от -90 до 90");
        }
    }
}