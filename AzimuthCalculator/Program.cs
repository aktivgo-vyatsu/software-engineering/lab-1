﻿using AzimuthCalculator;

Console.Write("Input source coordinate: ");
var input = Console.ReadLine()?.Split();
var source = new Point(float.Parse(input[0]), float.Parse(input[1]));

Console.Write("Input target coordinate: ");
input = Console.ReadLine()?.Split();
var target = new Point(float.Parse(input[0]), float.Parse(input[1]));

var (key, value) = AzimuthCalculator.AzimuthCalculator.Calculate(source, target);
Console.WriteLine("Result: " + (key, value));