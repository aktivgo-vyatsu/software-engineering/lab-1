namespace AzimuthCalculator;
using AzimuthValue = Double;

public enum AzimuthKey
{
    Concrete = 0,
    None = -1,
    Any = -2
}

public abstract class AzimuthCalculator
{
    private const int EarthRadius = 6371;

    public static Tuple<AzimuthKey, AzimuthValue> Calculate(Point source, Point target)
    {
        if (Math.Abs(target.Longitude - source.Longitude) == 180 && source.Latitude + target.Latitude == 0
            || Math.Abs(target.Latitude - source.Latitude) == 180)
        {
            return new Tuple<AzimuthKey, double>(AzimuthKey.Any, -2);
        }

        if (source.Longitude == target.Longitude && source.Latitude == target.Latitude)
        {
            return new Tuple<AzimuthKey, double>(AzimuthKey.None, -1);
        }
        
        if (Math.Abs(source.Latitude) == 90 && target.Latitude == source.Latitude)
        {
            return new Tuple<AzimuthKey, double>(AzimuthKey.None, -1);
        }

        if (Math.Abs(source.Latitude) == 90 || Math.Abs(target.Latitude) == 90)
        {
            return new Tuple<AzimuthKey, double>(AzimuthKey.Concrete, 180);
        }

        if (source.Longitude == target.Longitude)
        {
            if (source.Latitude - target.Latitude > 0)
            {
                return new Tuple<AzimuthKey, double>(AzimuthKey.Concrete, 0);
            }
            if (source.Latitude - target.Latitude < 0)
            {
                return new Tuple<AzimuthKey, double>(AzimuthKey.Concrete, 180);
            }
        }
        
        var azimuth = RadiansToDegrees(Math.Atan2(Math.Sin(DegreesToRadians(target.Longitude) - DegreesToRadians(source.Longitude)) * Math.Cos(DegreesToRadians(target.Latitude)),
            Math.Cos(DegreesToRadians(source.Latitude)) * Math.Sin(DegreesToRadians(target.Latitude)) - Math.Sin(DegreesToRadians(source.Latitude)) *
            Math.Cos(DegreesToRadians(target.Latitude)) * Math.Cos(DegreesToRadians(target.Longitude) - DegreesToRadians(source.Longitude))));
        if (azimuth < 0) azimuth += 360;

        return new Tuple<AzimuthKey, double>(AzimuthKey.Concrete, azimuth);
    }

    public static double GetDistance(Point source, Point target)
    {
        var a = (float)(Math.Pow(Math.Sin((DegreesToRadians(target.Latitude) - DegreesToRadians(source.Latitude)) / 2), 2) + 
                        Math.Cos(DegreesToRadians(source.Latitude)) * Math.Cos(DegreesToRadians(target.Latitude)) * 
                        Math.Pow(Math.Sin((DegreesToRadians(target.Longitude) - DegreesToRadians(source.Longitude)) / 2), 2));
        return 2 * EarthRadius * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
    }

    private static double DegreesToRadians(double degrees)
    {
        return degrees * Math.PI / 180;
    }

    private static double RadiansToDegrees(double radians)
    {
        return radians * 180 / Math.PI;
    }
}