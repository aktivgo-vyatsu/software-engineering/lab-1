using AzimuthCalculator;

namespace AzimuthCalculatorTest;

[TestClass]
public class AzimuthCalculatorTest
{
    [TestMethod]
    public void TestConstructor()
    {
        // arrange
        var cord1 = new Point(1, 1);
        var cord2 = new Point(20, 2);

        // act
        var result = AzimuthCalculator.AzimuthCalculator.Calculate(cord1, cord2);
        
        // assert
        Assert.IsNotNull(result);
    }

    [TestMethod]
    [DataRow(2, 2, 20, 20, AzimuthKey.Concrete, 43.07)]     // Correctness
    [DataRow(-30, -180, 30, 0, AzimuthKey.Any, -2)]         // Points is on straight through earth center
    [DataRow(1, 1, 1, 1, AzimuthKey.None, -1)]              // The same points
    [DataRow(90, 15, 15, 15, AzimuthKey.Concrete, 180)]     // One point is on north pole
    [DataRow(15, 15, 90, 15, AzimuthKey.Concrete, 180)]     // One point is on north pole vice versa
    [DataRow(90, 16, 90, 18, AzimuthKey.None, -1)]          // Both points are on north pole
    [DataRow(-90, 16, -90, 18, AzimuthKey.None, -1)]        // Both points are on south pole
    [DataRow(90, 15, 90, 15, AzimuthKey.None, -1)]          // Both points are on north pole and same
    [DataRow(90, 55, -90, -5, AzimuthKey.Any, -2)]          // Points on different poles
    [DataRow(-90, -5, 90, 55, AzimuthKey.Any, -2)]          // Points on different poles vice versa
    [DataRow(-90, 15, 15, 15, AzimuthKey.Concrete, 180)]    // One point is on south pole
    [DataRow(15, 15, -90, 15, AzimuthKey.Concrete, 180)]    // One point is on south pole vice versa
    [DataRow(30, 50, 50, 50, AzimuthKey.Concrete, 180)]     // Go to south on same meridian
    [DataRow(50, 50, -30, 50, AzimuthKey.Concrete, 0)]      // Go to north on same meridian
    [DataRow(0, 90, 0, -45, AzimuthKey.Concrete, 270)]      // If both points are on equator 270
    [DataRow(0, 40, 0, 50, AzimuthKey.Concrete, 90)]        // If both points are on equator 90
    public void TestAzimuthCalculationCorrectness(
        double latitude1, double longitude1,
        double latitude2, double longitude2,
        AzimuthKey expectedKey, double expectedValue)
    {
        // arrange
        var cord1 = new Point(latitude1, longitude1);
        var cord2 = new Point(latitude2, longitude2);
        var expected = (expectedKey, expectedValue);
        
        // act 
        var (key, value) = AzimuthCalculator.AzimuthCalculator.Calculate(cord1, cord2);
        
        // assert
        Assert.AreEqual(expected, (key, Math.Round(value, 2)));
    }

    [TestMethod]
    [DataRow(21, 2, 10, 120, 12418.1)] // Correctness
    [DataRow(10, 179, 10, -179, 219)] // Left and right of 180 meridian
    public void TestDistanceCalculationCorrectness(
        double latitude1, double longitude1,
        double latitude2, double longitude2,
        double expected)
    {
        // arrange
        var cord1 = new Point(latitude1, longitude1);
        var cord2 = new Point(latitude2, longitude2);
        
        // act 
        var result = AzimuthCalculator.AzimuthCalculator.GetDistance(cord1, cord2);
        
        // assert
        Assert.AreEqual(expected, Math.Round(result, 1));
    }
}