using AzimuthCalculator;

namespace AzimuthCalculatorTest;

[TestClass]
public class PointTest
{
    [TestMethod]
    public void TestConstructor()
    {
        var point = new Point(20.5, 1);
        Assert.IsNotNull(point);
    }
        
    [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void TestConstructorOutOfRange()
    {
        var _ = new Point(200, 200);
    }
}